import java.util.*;

class Project_1 {
    
    static Scanner input = new Scanner(System.in);
    static int W = 0, w = 0, I = 0, R = 0, choice = 0, exit = 0;

    public static void main(String[] args) {
        while(true){
            System.out.println("\t\t   Main Menu");
            System.out.println("1. Area and Volume");
            System.out.println("2. Tic Tac Toe");
            System.out.println("3. Matrix multiplication and determinant");
            System.out.println();
            System.out.println("0. Exit");
            System.out.println();
            do {
                try {
                    System.out.print("Enter the number above :::> ");
                    choice = input.nextInt();
                    if(choice>=-999999999 || choice<=999999999){
                        break;
                    }
                    break;
                }catch(Exception e) {
                    System.out.println("It isn't a number. Try again.\n");
                    input.nextLine();
                    continue;
                }
            }while(true);
            System.out.println();
            switch(choice){
                case 1:
                    luvol.main();
                    looping();
                    if(exit==0) {
                        break;
                    }
                    else if(exit==1) {
                        continue;
                    }
                    continue;
                case 2:
                    tictactoe.main();
                    looping();
                    if(exit==0) {
                        break;
                    }
                    else if(exit==1) {
                        continue;
                    }
                    continue;
                case 3:
                    matrix.main();
                    looping();
                    if(exit==0) {
                        break;
                    }
                    else if(exit==1) {
                        continue;
                    }
                    continue;
                case 0:
                    System.out.println("Thank you for your participation ^^");
                    break;
                default:
                    System.out.println("Invalid choice! Please try again.");
                    System.out.println();
                    continue;
            }
            break;
        }
    }
    static void looping() {
        while(true){
            System.out.println();
            System.out.println("Back to Main Menu? (Y/N)");
            System.out.print("::> ");
            char aw = input.next().charAt(0);
            if(aw=='Y' || aw=='y'){
                System.out.println();
                exit = 1;
                break;
            }
            else if(aw=='n' || aw=='N'){
                System.out.println("Thank you for your participation ^^");
                exit = 0;
                break;
            }
            else {
                System.out.print("Try again..\n");
                continue;
            }
        }
    }
    static class luvol extends Project_1 {
        static int luvol, twod, threed;
        static void main() {
            while(true) {
                System.out.println("\t\t   Area and Volume");
                System.out.println("1. Two-Dimentional figure.");
                System.out.println("2. Geometry.\n");
                System.out.println("0. Back\n");
                do {
                    try {
                        System.out.print("Your choice :::> ");
                        luvol = input.nextInt();
                        if(luvol>=-999999999 || luvol<=999999999){
                            break;
                        }
                        break;
                    }catch(Exception e) {
                        System.out.println("It isn't a number. Try again.\n");
                        input.nextLine();
                    }
                }while(true);
                System.out.println();
                switch(luvol) {
                    case 1:
                        TwoD.main();
                        break;
                    case 2:
                        ThreeD.main();
                        break;
                    case 0:
                        System.out.println();
                        break;
                    default:
                        System.out.println("Invalid choice. Please try again.\n");
                        continue;
                }
                break;
            }
        }
        static class TwoD extends luvol {
            static int p, l, d1, d2, a, t, r, k, j, A, cho;
            static void main() {
                int L = 0;
                while(L==0) {
                    System.out.println("Two Dimentional Figure");
                    System.out.println("1. Square");
                    System.out.println("2. Rectangle");
                    System.out.println("3. Triangle");
                    System.out.println("4. Circle");
                    System.out.println("5. Kite");
                    System.out.println("6. Rhombus");
                    System.out.println("7. Parallelogram");
                    System.out.println("8. Trapezoid");
                    System.out.println();
                    System.out.println("0. Main Menu\n");
                    do {
                        I=0;
                        try {
                            System.out.print("Your choice :::> ");
                            twod = input.nextInt();
                            if(twod>=-999999999 || twod<=999999999){
                                I=1;
                            }
                        }
                        catch(Exception e) {
                            System.out.println("It isn't a number. Try again.\n");
                            input.nextLine();
                        }
                    }
                    while(I==0);
                    System.out.println();
                    switch(twod) {
                        case 1:
                            satuin(p, "length");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nLength = "+p);
                                System.out.println("\nThe area of the square is = "+area1(p,p));
                            }
                            else if(A==2) {
                                System.out.println("\nLength = "+p);
                                System.out.println("\nThe perimeter of the square is = "+perimeter1(p,p));
                            }
                            L = 1;
                            break;
                        case 2:
                            satuin(p, "length");
                            satuin(l, "width");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nLength = "+p);
                                System.out.println("Width = "+l);
                                System.out.println("\nThe area of the rectangle is = "+area1(p,l));
                            }
                            else if(A==2) {
                                System.out.println("\nLength = "+p);
                                System.out.println("Width = "+l);
                                System.out.println("\nThe perimeter of the rectangle is = "+perimeter1(p,l));
                            }
                            L = 1;
                            break;
                        case 3:
                            satuin(a, "base");
                            satuin(t, "height");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nBase = "+a);
                                System.out.println("Height = "+t);
                                System.out.println("\nThe area of the triangle is = "+area1(a,t));
                            }
                            else if(A==2) {
                                System.out.println("\nBase = "+a);
                                System.out.println("Height = "+t);
                                int m = (int) Math.sqrt(Math.pow(a, 2)+Math.pow(t, 2));
                                System.out.println("Length = square root of ("+a+"^2 + "+t+"^2) = "+m);
                                System.out.println("\nThe perimeter of the triangle is = "+perimeter1(a,t));
                            }
                            L = 1;
                            break;
                        case 4:
                            satuin(r, "radius");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nRadius = "+r);
                                System.out.println("\nThe area of the circle is = "+area1((r*r), 3.142857142857143));
                            }
                            else if(A==2) {
                                System.out.println("\nRadius = "+r);
                                System.out.println("Diameter = "+(r*2));
                                System.out.println("\nThe perimeter of the rectangle is = "+area1((r*2), 3.142857142857143));
                            }
                            L = 1;
                            break;
                        case 5:
                            satuin(d1, "diagonal 1");
                            satuin(d2, "diagonal 2");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nDiagonal 1 = "+d1);
                                System.out.println("Diagonal 2 = "+d2);
                                System.out.println("\nThe area of the kite is = "+(0.5*(area1(d1,d2))));
                            }
                            else if(A==2) {
                                satuin(k, "length of the side 1");
                                satuin(j, "length of the side 2");
                                System.out.println("\nDiagonal 1= "+d1);
                                System.out.println("Diagonal 2 = "+d2);
                                System.out.println("One of the side length = "+k);
                                System.out.println("Another side length = "+j);
                                System.out.println("\nThe perimeter of the triangle is = "+perimeter1(k,j));
                            }
                            L = 1;
                            break;
                        case 6:
                            satuin(d1, "diagonal 1");
                            satuin(d2, "diagonal 2");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nDiagonal 1 = "+d1);
                                System.out.println("Diagonal 2 = "+d2);
                                System.out.println("\nThe area of the kite is = "+(0.5*(area1(d1,d2))));
                            }
                            else if(A==2) {
                                System.out.println("\nDiagonal 1= "+d1);
                                System.out.println("Diagonal 2 = "+d2);
                                int m = (int) Math.sqrt(Math.pow(((double)(d1/2)), 2)+Math.pow(((double) (d2/2)), 2));
                                System.out.println("Length = square root of ("+(d1/2)+"^2 + "+(d2/2)+"^2) = "+m);
                                System.out.println("\nThe perimeter of the triangle is = "+perimeter1(m,m));
                            }
                            L = 1;
                            break;
                        case 7:
                            satuin(a, "base");
                            satuin(t, "height");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nBase = "+a);
                                System.out.println("Height = "+t);
                                System.out.println("\nThe area of the parallelogram is = "+area1(a,t));
                            }
                            else if(A==2) {
                                satuin(k, "length of the side 1");
                                System.out.println("\nBase = "+a);
                                System.out.println("Height = "+t);
                                System.out.println("The tilt side length = "+k);
                                System.out.println("\nThe perimeter of the parallelogram is = "+perimeter1(a,k));
                            }
                            L = 1;
                            break;
                        case 8:
                            satuin(a, "base 1");
                            satuin(p, "base 2");
                            satuin(t, "height");
                            choice(A);
                            if(A==1) {
                                System.out.println("\nBase 1 = "+a);
                                System.out.println("Base 2 = "+p);
                                System.out.println("Height = "+t);
                                System.out.println("\nThe area of the trapezoid is = "+area2(a, p, t));
                            }
                            else if(A==2) {
                                satuin(k, "length of the side 1");
                                System.out.println("\nBase 1 = "+a);
                                System.out.println("Base 2 = "+p);
                                System.out.println("Height = "+t);
                                System.out.println("The tilt side length = "+k);
                                System.out.println("\nThe perimeter of the trapezoid is = "+perimeter1(a,k));
                            }
                            L = 1;
                            break;
                        case 0:
                            L = 1;
                            break;
                        default:
                            System.out.println("Invalid choice. Please try again.\n");
                    }
                }
            }
            static int satuin(int p, String a) {
                exception(p, a);
                return p;
            }
            static void exception(int c, String b) {
                do {
                    R=0;
                    try {
                        System.out.print("Please type the "+b+" :::> ");
                        c = input.nextInt();
                        if(c<0) {
                            throw new ArithmeticException();
                        }
                        else if(c>0) {
                            if(b=="length"|| b=="base 2") {
                                p = c;
                            }
                            else if(b=="width") {
                                l = c;
                            }
                            else if(b=="base" || b=="base 1") {
                                a = c;
                            }
                            else if(b=="height") {
                                t = c;
                            }
                            else if(b=="radius") {
                                r = c;
                            }
                            else if(b=="diagonal 1") {
                                d1 = c;
                            }
                            else if(b=="diagonal 2") {
                                d2 = c;
                            }
                            else if(b=="length of the side 1") {
                                k = c;
                            }
                            else if(b=="length of the side 2") {
                                j = c;
                            }
                            R=1;
                        }
                        else if(c==0) {
                            throw new IllegalArgumentException();
                        }
                    }
                    catch(InputMismatchException e) {
                        System.out.println("It isn't a number. Try again.");
                        input.nextLine();
                    }
                    catch(ArithmeticException e) {
                        System.out.println("The number can't be a negative. Try again.");
                    }
                    catch(IllegalArgumentException e) {
                        System.out.println("The number can't be a zero. Try again.");
                    }
                }
                while(R==0);
            }
            static int choice(int g) {
                System.out.println("\n1. Area");
                System.out.println("2. Perimeter\n");
                do {
                    I=0;
                    try {
                        System.out.print("Your choice :::> ");
                        g = input.nextInt();
                        if(g>0 && g<3){
                            I=1;
                        }
                        else if(g<=0 || g>3) {
                            throw new IllegalArgumentException();
                        }
                    }
                    catch(InputMismatchException e) {
                        System.out.println("It isn't a number. Try again.\n");
                        input.nextLine();
                    }
                    catch(IllegalArgumentException e) {
                        System.out.println("Please type a numbers between 1-2.\n");
                    }
                }
                while(I==0);
                A = g;
                return A;
            }
            static double area1(double p, double l) {
              return(p*l);  
            }
            static double area2(double p, double r, double s) {
                return (((0.5)*(p+r))*s);
            }
            static double perimeter1(double p, double l) {
                return(2*p+2*l);
            }
            static double perimeter2(double p, double r, double s, double t) {
                return (p+r+s+t);
            }
        }
        static class ThreeD extends luvol {
            static int A, cho, l, w, h, r, a, H, B;
            static double phi = 3.142857142857143, three = 0.3333333333333333, four = 1.333333333333333;
            static void main() {
                int L = 0;
                while(L==0) {
                    System.out.println("Geometry");
                    System.out.println("1. Cube");
                    System.out.println("2. Cuboid");
                    System.out.println("3. Triangular Pyramid");
                    System.out.println("4. Square Pyramid");
                    System.out.println("5. Triangular Prism");
                    System.out.println("6. Cylinder");
                    System.out.println("7. Cone");
                    System.out.println("8. Sphere");
                    System.out.println();
                    System.out.println("0. Main Menu\n");
                    do {
                        I=0;
                        try {
                            System.out.print("Your choice :::> ");
                            threed = input.nextInt();
                            if(threed>=-999999999 || threed<=999999999){
                                I=1;
                            }
                        }
                        catch(Exception e) {
                            System.out.println("It isn't a number. Try again.\n");
                            input.nextLine();
                        }
                    }
                    while(I==0);
                    System.out.println();
                    switch(threed) {
                        case 1:
                            choice(A);
                            if(A==1) {
                                satuin(l, "side length");
                                System.out.println("\nThe side of the cube = "+l);
                                System.out.println("Volume formula for the cube = side^3");
                                System.out.println("\nVolume = "+mainformula(l, l, l, B));
                            }
                            else if (A==2) {
                                satuin(l, "side length");
                                System.out.println("\nThe side of the cube = "+l);
                                System.out.println("Surface area formula for the cube = 6*(side^2)");
                                System.out.println("\nSurface area = "+cubes(l, B));
                            }
                            L = 1;
                            break;
                        case 2:
                            choice(A);
                            if(A==1) {
                                satuin(l, "length");
                                satuin(w, "width");
                                satuin(h, "height");
                                System.out.println("\nThe length of the cuboid = "+l);
                                System.out.println("The width of the cuboid = "+w);
                                System.out.println("The height of the cuboid = "+h);
                                System.out.println("Volume formula for the cuboid = length * width * height");
                                System.out.println("\nVolume = "+mainformula(l, w, h, B));
                            }
                            else if (A==2) {
                                satuin(l, "length");
                                satuin(w, "width");
                                satuin(h, "height");
                                System.out.println("\nThe length of the cuboid = "+l);
                                System.out.println("The width of the cuboid = "+w);
                                System.out.println("The height of the cuboid = "+h);
                                System.out.println("Surface area formula for the cuboid = 2*(length * width + width * height + height * length)");
                                System.out.println("\nSurface area = "+(2*area(l, w, B)+2*area(w, h, B)+2*area(h, l, B)));
                            }
                            L = 1;
                            break;
                        case 3:
                            choice(A);
                            if(A==1) {
                                satuin(a, "base");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base length of the triangular pyramid = "+a);
                                System.out.println("The height of the triangular pyramid = "+h);
                                System.out.println("The surface height of the triangular pyramid = "+H);
                                System.out.println("Volume formula for the triangular pyramid = (base * height * surface height) / 6");
                                System.out.println("\nVolume = "+(mainformula(a, h, H, B)*three*0.5));
                            }
                            else if (A==2) {
                                satuin(a, "base");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base length of the triangular pyramid = "+a);
                                System.out.println("The height of the triangular pyramid = "+h);
                                System.out.println("The surface height of the triangular pyramid = "+H);
                                System.out.println("Surface area formula for the triangular pyramid = lateral surface area + area of the base");
                                System.out.println("\nSurface area = "+(0.5*area(a, h, B)+1.5*area(a, H, B)));
                            }
                            L = 1;
                            break;
                        case 4:
                            choice(A);
                            if(A==1) {
                                satuin(l, "side length");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base side length of the rectangle pyramid = "+l);
                                System.out.println("The height of the rectangle pyramid = "+h);
                                System.out.println("The surface height of the rectangle pyramid = "+H);
                                System.out.println("Volume formula for the rectangle pyramid = (base side * height * surface height) / 3");
                                System.out.println("\nVolume = "+(mainformula(l, h, H, B)*three));
                            }
                            else if (A==2) {
                                satuin(l, "side length");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base side length of the rectangle pyramid = "+l);
                                System.out.println("The height of the rectangle pyramid = "+h);
                                System.out.println("The surface height of the rectangle pyramid = "+H);
                                System.out.println("Surface area formula for the rectangle pyramid = lateral surface area + area of the base");
                                System.out.println("\nSurface area = "+(area(l, l, B)+2*area(l, H, B)));
                            }
                            L = 1;
                            break;
                        case 5:
                            choice(A);
                            if(A==1) {
                                satuin(a, "base");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base side length of the triangular prism = "+a);
                                System.out.println("The height of the triangular prism = "+h);
                                System.out.println("The surface height of the triangular prism = "+H);
                                System.out.println("Volume formula for the triangular prism = (base side * height * surface height) / 2");
                                System.out.println("\nVolume = "+(mainformula(a, h, H, B)*0.5));
                            }
                            else if (A==2) {
                                satuin(a, "base");
                                satuin(h, "height");
                                satuin(H, "surface height");
                                System.out.println("\nThe base side length of the triangular prism = "+a);
                                double m = Math.sqrt(Math.pow((a/2), 2)+Math.pow(h, 2));
                                System.out.println("The phytagoras base side length of the triangular prism = "+m);
                                System.out.println("The height of the triangular prism = "+h);
                                System.out.println("The surface height of the triangular prism = "+H);
                                System.out.println("Surface area formula for the triangular prism = lateral surface area + 2 * area of the base");
                                System.out.println("\nSurface area = "+(area(a, h, B)+(a + m + m)*H));
                            }
                            L = 1;
                            break;
                        case 6:
                            choice(A);
                            if(A==1) {
                                satuin(r, "radius");
                                satuin(H, "surface height");
                                System.out.println("\nThe radius of the cylinder = "+r);
                                System.out.println("The height of the cylinder = "+H);
                                System.out.println("Volume formula for the cylinder = phi * r^2 * H");
                                System.out.println("\nVolume = "+mainformula(phi, Math.pow(r, 2), H, B));
                            }
                            else if (A==2) {
                                satuin(r, "radius");
                                satuin(H, "surface height");
                                System.out.println("\nThe radius of the cylinder = "+r);
                                System.out.println("The height of the cylinder = "+H);
                                System.out.println("Surface area formula for the cylinder = 2 * phi * r * H + 2 * phi * r^2");
                                System.out.println("\nSurface area = "+(2 * mainformula(phi, r , H, B)+(mainformula(2, phi, Math.pow(r, 2), B))));
                            }
                            L = 1;
                            break;
                        case 7:
                            choice(A);
                            if(A==1) {
                                satuin(r, "radius");
                                satuin(H, "surface height");
                                System.out.println("\nThe radius of the cone = "+r);
                                System.out.println("The height of the cone = "+H);
                                System.out.println("Volume formula for the cone = 1/3 * phi * r^2 * H");
                                System.out.println("\nVolume = "+(three * mainformula(phi, Math.pow(r, 2), H, B)));
                            }
                            else if (A==2) {
                                satuin(r, "radius");
                                satuin(H, "surface height");
                                satuin(l, "slant height");
                                System.out.println("\nThe radius of the cone = "+r);
                                System.out.println("The height of the cone = "+H);
                                System.out.println("The slant height of the cone = "+l);
                                System.out.println("Surface area formula for the cone = phi * r * l + phi * r^2");
                                System.out.println("\nSurface area = "+(mainformula(phi, r , l, B) + area(phi, Math.pow(r, 2), B)));
                            }
                            L = 1;
                            break;
                        case 8:
                            choice(A);
                            if(A==1) {
                                satuin(r, "radius");
                                System.out.println("\nThe radius of the sphere = "+r);
                                System.out.println("Volume formula for the sphere = 4/3 * phi * r^3");
                                System.out.println("\nVolume = "+mainformula(four, phi, Math.pow(r, 3), B));
                            }
                            else if (A==2) {
                                satuin(r, "radius");
                                System.out.println("\nThe radius of the sphere = "+r);
                                System.out.println("Surface area formula for the sphere = 4 * phi * r^2");
                                System.out.println("\nSurface area = "+mainformula(4, phi , Math.pow(r, 2), B));
                            }
                            L = 1;
                            break;
                        case 0:
                            L = 1;
                            break;
                        default:
                            System.out.println("Invalid choice. Please try again.\n");
                            break;
                    }
                }
            }
            static int choice(int g) {
                System.out.println("1. Volume");
                System.out.println("2. Surface Area\n");
                do {
                    I=0;
                    try {
                        System.out.print("Your choice :::> ");
                        g = input.nextInt();
                        if(g>0 && g<3){
                            I=1;
                        }
                        else if(g<=0 || g>3) {
                            throw new IllegalArgumentException();
                        }
                    }
                    catch(InputMismatchException e) {
                        System.out.println("It isn't a number. Try again.\n");
                        input.nextLine();
                    }
                    catch(IllegalArgumentException e) {
                        System.out.println("Please type a numbers between 1-2.\n");
                    }
                }
                while(I==0);
                A = g;
                return A;
            }
            static int satuin(int p, String a) {
                exception(p, a);
                return p;
            }
            static void exception(int c, String b) {
                do {
                    R=0;
                    try {
                        System.out.print("Please type the "+b+" :::> ");
                        c = input.nextInt();
                        if(c<0) {
                            throw new ArithmeticException();
                        }
                        else if(c>0) {
                            if(b=="length"|| b=="side length" || b=="slant height") {
                                l = c;
                            }
                            else if(b=="base") {
                                a = c;
                            }
                            else if(b=="width") {
                                w = c;
                            }
                            else if(b=="surface height") {
                                H = c;
                            }
                            else if(b=="height") {
                                h = c;
                            }
                            else if(b=="radius") {
                                r = c;
                            }
                            R=1;
                        }
                        else if(c==0) {
                            throw new IllegalArgumentException();
                        }
                    }
                    catch(InputMismatchException e) {
                        System.out.println("It isn't a number. Try again.");
                        input.nextLine();
                    }
                    catch(ArithmeticException e) {
                        System.out.println("The number can't be a negative. Try again.");
                    }
                    catch(IllegalArgumentException e) {
                        System.out.println("The number can't be a zero. Try again.");
                    }
                }
                while(R==0);
            }
            static double mainformula(double p, double l, double k, double h) {
                h = p*l*k;
                return h;
            }
            static double cubes(int p, int h) {
                h = 6*(p*p);
                return h;
            }
            static double area(double p, double l, double h) {
                h = p*l;
                return h;
            }
        }
    }
    static class tictactoe extends Project_1 {
        static int M = 3, counter = 9, x = 0, row = 0, col = 0;
        static char [][]board = new char [M][M];
        static void main() {
            x = 0;
            boolean win = false;
            System.out.println("\t\t   Tic Tac Toe");
            System.out.println();
            System.out.print("Enter player 1 name :::> ");
            String player1 = input.next();
            System.out.print("Enter player 2 name :::> ");
            String player2 = input.next();
            System.out.println();
            System.out.println(player1+" = X");
            System.out.println(player2+" = O");
            board();
            while(win == false){
                while(x<counter){
                    int a = 0;
                    for(a = 0; a<1; a++){
                        int rows = 0, cols = 0;
                        System.out.println("-----------------------");
                        System.out.println(player1+" turn");
                        System.out.println("-----------------------");
                        int e = 0, f = 0;
                        while(e==0) {
                            exception(row, "row");
                            rows = row - 1;
                            e = 1;
                        }
                        while(f==0) {
                            exception(col, "col");
                            cols = col - 1;
                            f = 1;
                        }
                        turn(board, player1, 'X', rows, cols);
                        show();
                        win(board, player1, 'X', rows, cols);
                        int b = 0;
                        x++;
                        while(x<counter) {
                            for(b = 0; b<1; b++) {
                                System.out.println("-----------------------");
                                System.out.println(player2+" turn");
                                System.out.println("-----------------------");
                                int E = 0, F = 0;
                                while(E==0) {
                                    exception(row, "row");
                                    rows = row - 1;
                                    E = 1;
                                }
                                while(F==0) {
                                    exception(col, "col");
                                    cols = col - 1;
                                    F = 1;
                                }
                                turn(board, player2, 'O', rows, cols);
                                show();
                                win(board, player2, 'O', rows, cols);
                                x++;
                            }
                            break;
                        }
                    }
                }
                if(x==counter && win==false){
                    System.out.println("Draw.");
                    win = true;
                }
                win = true;
            }
        }
        static void board() {
            for(int x = 0; x<M; x++) {
                for(int y = 0; y<M; y++) {
                    board[x][y] = ' ';
                }
            }
        }
        static void show() {
            System.out.println("Tic Tac Toe Board\n");
            for(int x = 0; x<M; x++) {
                for(int y = 0; y<M; y++) {
                    System.out.print("   "+board[x][y]+"   ");
                    if(y<(M-1)) {
                        System.out.print("|");
                    }
                }
                if(x<(M-1)) {
                    System.out.println("\n-----------------------\n");
                }
                else {
                    System.out.println();
                    System.out.println();
                }
            }
        }
        static void turn(char[][] board, String player,char XO,int rows, int cols) {
            while ((board[rows][cols] != ' ')||rows<0||rows>2||cols<0||cols>2) {
                System.out.println("Please type again");
                exception(row, "row");
                rows = row-1;
                exception(col, "col");
                cols = col-1;
            }
            board[rows][cols] = XO;
        }
        static boolean win(char[][] board,String player,char XO, int row, int col) {
            if (board[0][col]==board[1][col] && board[0][col]==board[2][col] && board[0][col]==XO) {
                System.out.println(player+" has won!");
                x = 9;
                return true;
            }
            if (board[row][0]==board[row][1] && board[row][0]==board[row][2] && board[row][0]==XO) {
                System.out.println(player+" has won!");
                x = 9;
                return true;
            }
            if (board[0][0]==board[1][1] && board[0][0]==board[2][2] && board[0][0]==XO) {
                System.out.println(player+" has won!");
                x = 9;
                return true;
            }
            if (board[2][0]==board[1][1] && board[row][col]==board[0][2] && board[2][0]==XO) {
                System.out.println(player+" has won!");
                x = 9;
                return true;
            }
            return false;
        }
        static void exception(int a, String b) {
            do {
                R=0;
                try {
                    System.out.print("Please type a "+b+" :::> ");
                    a = input.nextInt();
                    if(a<=0 || a>3) {
                        throw new ArithmeticException();
                    }
                    else if(a>0 && a<=3) {
                        if(b=="row") {
                            row = a;
                        }
                        else if(b=="col") {
                            col = a;
                        }
                        R=1;
                    }
                }
                catch(InputMismatchException e) {
                    System.out.println("It isn't a number. Try again.");
                    input.nextLine();
                }
                catch(ArithmeticException e) {
                    System.out.println("Please type a number between 1-3.");
                }
            }
            while(R==0);
        }
    }
    static class matrix extends Project_1 {
        static void main(){
            while(true){
                int select;
                System.out.println("\t\t   MATRIX\n");
                System.out.println("1. Addition");
                System.out.println("2. Subtraction");
                System.out.println("3. Multiplication");
                System.out.println();
                System.out.println("0. Exit");
                System.out.println("Please select a number");
                do{
                    System.out.print("::> ");
                    try{
                        select=input.nextInt();
                        if(select!= 1 && select!= 2 && select!= 3  && select!= 0 ){
                            System.out.println("Please select only the '1', '2', '3', or '0' number");
                            continue;
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please select only the '1', '2', '3', or '0' number");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                if(select== 0){
                    break;
                }
                switch(select){
                    case 1:
                        addition();
                        break;
                    case 2:
                        subtraction();
                        break;
                    case 3:
                        multiplication();
                        break;
                }
            }
        }
        static void addition(){
            int rows,columns;
            do{
                System.out.println("\t\t  Addition");
                System.out.println();
                do{
                    try{
                        System.out.println("Input the number of rows of the matrices ");
                        System.out.print("::> ");
                        rows=input.nextInt();
                        if (rows<1){
                            throw new InputMismatchException();
                        }
                        System.out.println("Input the number of columns of the matrices ");
                        System.out.print("::> ");
                        columns=input.nextInt();
                        if (columns<1){
                            throw new InputMismatchException();
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a natural number ( > 0 )");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                double[][]matrix1= new double[rows][columns];
                double[][]matrix2= new double[rows][columns];
                double[][]matrixResult=new double[rows][columns];
                System.out.println("\t\t MATRIX A");
                do{
                    try{
                        for(int rowindex=0; rowindex<rows;rowindex++){
                            for(int columnindex=0; columnindex<columns;columnindex++){
                                System.out.println("Input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
                                System.out.print("::> ");
                                matrix1[rowindex][columnindex]=input.nextDouble();
                            }
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a real number");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrix1[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                System.out.println();
                System.out.println("\t\t MATRIX B");
                do{
                    try{
                        for(int rowindex=0; rowindex<rows;rowindex++){
                            for(int columnindex=0; columnindex<columns;columnindex++){
                                System.out.println("Please input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
                                System.out.print("::> ");
                                matrix2[rowindex][columnindex]=input.nextDouble();
                                matrixResult[rowindex][columnindex]=matrix1[rowindex][columnindex] + matrix2[rowindex][columnindex];
                            }
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a real number");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrix2[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                System.out.println("\n");
                System.out.println("The result of the addition of both matrices is ");
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrixResult[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                if(rows != columns){
                    System.out.println("The result of the matrix doesn't have a determinant");
                }
                else{
                    if(rows==2 && columns==2){
                        if(((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0]))!=0){
                            System.out.println("The determinant of the matrix is "+((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0])));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                    else if(rows==3 && columns==3){
                        if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
                            System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                    -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                    +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                }
                System.out.println();
                break;
            }while(true);
        }
        static void subtraction(){
            int rows,columns;
            do{
                System.out.println("\t\t  Subtraction");
                System.out.println();
                do{
                    try{
                        System.out.println("Input the number of rows of the matrices ");
                        System.out.print("::> ");
                        rows=input.nextInt();
                        if (rows <1){
                            throw new InputMismatchException();
                        }
                        System.out.println("Input the number of columns of the matrices ");
                        System.out.print("::> ");
                        columns=input.nextInt();
                        if (columns <1){
                            throw new InputMismatchException();
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a natural number ( > 0 )");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                double[][]matrix1= new double[rows][columns];
                double[][]matrix2= new double[rows][columns];
                double[][]matrixResult=new double[rows][columns];
                System.out.println("\t\t MATRIX A");
                do{
                    try{
                        for(int rowindex=0; rowindex<rows;rowindex++){
                            for(int columnindex=0; columnindex<columns;columnindex++){
                                System.out.println("Input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
                                System.out.print("::> ");
                                matrix1[rowindex][columnindex]=input.nextDouble();
                            }
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a real number");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrix1[rowindex][columnindex]);
                    }System.out.println();
                }
                System.out.println();
                System.out.println("\t\t MATRIX B");
                do{
                    try{
                        for(int rowindex=0; rowindex<rows;rowindex++){
                            for(int columnindex=0; columnindex<columns;columnindex++){
                                System.out.println("Please input the value of row "+(rowindex+1)+" and column "+(columnindex+1));
                                System.out.print("::> ");
                                matrix2[rowindex][columnindex]=input.nextDouble();
                                matrixResult[rowindex][columnindex]= matrix1[rowindex][columnindex] - matrix2[rowindex][columnindex];
                            }
                        }
                        break;
                    }catch(InputMismatchException e){
                        System.out.println("Please input a real number");
                        input.nextLine();
                        continue;
                    }
                }while(true);
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrix2[rowindex][columnindex]);
                    }System.out.println();
                }
                System.out.println("\n");
                System.out.println("The result of the subtraction of both matrices is ");
                System.out.println();
                for(int rowindex=0; rowindex<rows;rowindex++){
                    for(int columnindex=0; columnindex<columns;columnindex++){
                        System.out.printf("%.2f  ",matrixResult[rowindex][columnindex]);
                    }System.out.println();
                }
                if(rows != columns){
                    System.out.println("The result of the matrix doesn't have a determinant");
                }
                else{
                    if(rows==2 && columns==2){
                        if(((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0]))!=0){
                            System.out.println("The determinant of the matrix is "+((matrixResult[0][0]*matrixResult[1][1])-(matrixResult[0][1]*matrixResult[1][0])));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                    else if(rows==3 && columns==3){
                        if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
                            System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                    -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                    +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                }
                System.out.println();
                break;
            }while(true);
        }
        static void multiplication(){
            while(true){
                int matrixMultiTotal=0,choice=0,rows1,columns1,rows2,columns2;
                do{
                    do{
                        try{
                            System.out.print("The number of rows of matrix A : ");
                            rows1=input.nextInt();
                            System.out.print("The number of columns of matrix A : ");
                            columns1=input.nextInt();
                            System.out.print("The number of rows of matrix B : ");
                            rows2=input.nextInt();
                            System.out.print("The number of columns of matrix B : ");
                            columns2=input.nextInt();
                            break;
                        }catch(InputMismatchException e){
                            System.out.println("Please input an integer");
                            input.nextLine();
                            continue;
                        }
                    }while(true);
                    if (columns1 != rows2) {
                        System.out.println("The number of rows of matrix A does not equal the number of columns of matrix B");
                        System.out.println("Matrices cannot be multiplied");
                        do{
                            try{
                                System.out.print("Press '0' to repeat the process or '1' to terminate : ");
                                choice=input.nextInt();
                                if(choice==1)
                                    break;
                                if(choice==0)
                                    break;
                            }catch(InputMismatchException e){
                                System.out.println("Input one of the given number");
                                input.nextLine();
                                continue;
                            }
                        }while((choice!=0)||(choice!=1));
                    }
                    if(choice==1)
                        break;
                    if(choice==0)
                        continue;
                }while(columns1!=rows2);
                if(choice==1)
                    break;
                System.out.println("\n");
                int[][]matrix=new int[rows1][columns1];
                do{
                    System.out.println("Matrix A: ");
                    for(int rowindex=0;rowindex<rows1;rowindex++){
                        for(int columnindex=0;columnindex<columns1;columnindex++){
                            System.out.print("Row "+(rowindex+1)+" column "+(columnindex+1)+" : ");
                            try{
                                matrix[rowindex][columnindex]=input.nextInt();
                            }catch(InputMismatchException e){
                                System.out.println("Please input only an integer");
                                input.nextLine();
                                columnindex--;
                                continue;
                            }
                        }
                    }
                    break;
                }while(true);
                System.out.println();
                for(int rowindex=0;rowindex<rows1;rowindex++){
                    for(int columnindex=0;columnindex<columns1;columnindex++){
                        System.out.printf("%4d ", matrix[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                System.out.println();
                int[][]matrix2=new int[rows2][columns2];
                do{
                    System.out.println("Matrix B: ");
                    for(int rowindex=0;rowindex<rows2;rowindex++){
                        for(int columnindex=0;columnindex<columns2;columnindex++){
                            System.out.print("row "+(rowindex+1)+" column "+(columnindex+1)+" : ");
                            try{
                                matrix2[rowindex][columnindex]=input.nextInt();
                            }catch(InputMismatchException e){
                                System.out.println("Please input only an integer");
                                input.nextLine();
                                columnindex--;
                                continue;
                            }
                        }
                    }
                    break;
                }while(true);
                System.out.println();
                for(int rowindex=0;rowindex<rows2;rowindex++){
                    for(int columnindex=0;columnindex<columns2;columnindex++){
                        System.out.printf("%4d ", matrix2[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                int[][]matrix3=new int[rows1][columns2];
                for(int rowindex=0;rowindex<rows1;rowindex++){
                    for(int columnindex=0;columnindex<columns2;columnindex++){
                        matrixMultiTotal=0;
                        for(int multiplicationindex=0;multiplicationindex<columns1;multiplicationindex++){
                            matrixMultiTotal+=matrix[rowindex][multiplicationindex]*matrix2[multiplicationindex][columnindex];
                        }
                        matrix3[rowindex][columnindex]=matrixMultiTotal;
                    }
                }
                System.out.println();
                System.out.println("The multiplication of matrices "+rows1+"x"+columns1+" X "+rows2+"x"+columns2+" will make up "+rows1+"x"+columns2+" matrix");
		System.out.println("The result of the multiplication of both matrices is :\n");
                for(int rowindex=0;rowindex<rows1;rowindex++){
                    for(int columnindex=0;columnindex<columns2;columnindex++){
                        System.out.printf("%4d ", matrix3[rowindex][columnindex]);
                    }
                    System.out.println();
                }
                System.out.println("\n");
                if(rows1 != columns2){
                    System.out.println("The result of the matrix doesn't have a determinant");
                }
                else{
                    if(rows1==2 && columns2==2){
                        if(((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0]))!=0){
                            System.out.println("The determinant of the matrix is "+((matrix3[0][0]*matrix3[1][1])-(matrix3[0][1]*matrix3[1][0])));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                    else if(rows1==3 && columns2==3){
                        if(((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1]))))!=0){
                            System.out.println("The determinant of the matrix is "+((matrix2[0][0]*((matrix2[1][1]*matrix2[2][2])-(matrix2[2][1]*matrix2[1][2])))
                                    -(matrix2[0][1]*((matrix2[1][0]*matrix2[2][2])-(matrix2[2][0]*matrix2[1][2])))
                                    +(matrix2[0][2]*((matrix2[1][0]*matrix2[2][1])-(matrix2[2][0]*matrix2[1][1])))));
                        }
                        else{
                            System.out.println("It's a mirrored matrix");
                        }
                    }
                }
                break;
            }
        }
    }
}